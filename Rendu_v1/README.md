# Dossier du premier rendu

Le dossier contient les différents livrables demandés pour la v1 de notre projet. C'est à dire : 
- README (présent à la racine)
- NDC : Note de clarification
- MCD : Modèle Conceptuel de Données
- MLD relationnel : Modèle Logique de Données
- BDD : tables et vues, données de test, questions attendues (vues)

