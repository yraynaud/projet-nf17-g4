# Note de clarification

## Objets et propriétés associées

### Ressources
- code : unique 
- titre : non nul
- date_apparition : non nulle
- editeur : non nul
- genre : non nul
- code_classification : non nul

#### Livres
- auteur
- isbn : unique 
- resume
- langue 

#### Films
- realisateurs
- acteurs
- langue 
- duree : en minutes
- synopsis

#### Musiques
- compositeurs
- interpretes
- duree : en minutes

### Contributeurs
- id : (primary)
- nom
- prenom
- date_naissance
- nationalite

### Contribue
- id_contrib : (primary)
- id_ressource : (primary)
- role : (primary) : enum {auteur, compositeur, interprète, réalisateur, acteur}

### Éditeurs
- nom : (primary)

### Genres
- nom : (primary)

### Exemplaires
- id : (local)
- etat : enum {neuf, bon, abîmé, perdu}

### Comptes
- login : (primary)
- mot_de_passe
- nom
- prenom
- adresse
- email

### Adhérents
- date_naissance
- numero_telephone
- date_fin_adhesion
- blackliste : booléen

### Personnels

### Prets
- id_ressource : (primary)
- id_exemplaire : (primary)
- login_adhérant : (primary)
- date_pret : date
- duree_pret
- date_rendu : date

### Sanctions
- type : enum {perte, dégradation, retard)

### Temporaires
- duree de sanction
- date

### À rembourser
- rembourse : booléen


## Contraintes sur les objets

- **Contribue**, **Editeurs** et **Genres** sont associés à **Ressources**
- **Contribue** est associé à **Contributeurs**
- **Ressources** est composé d'**Exemplaires**
- **Prets** est associé à **Exemplaires** et **Adherents**
- **Sanctions** est associé à **Prets**
- **Temporaires** et **A_rembourser** héritent de **Sanctions**
- **Films** **Musiques**  et  **Livres** héritent de **Ressources**
- **Adherents** et **Personnels** héritent de **Comptes**


## Utilisateurs
Administateur : A les mêmes droits que le personnel mais peut en plus créer de nouveaux comptes pour le personnel.

Personnel : Peut gérer les emprunts, les comptes des adhérents et les ressources.

Adhérent : Peut voir ses informations personnelles et ses emprunts / sanctions.

## Fonctions
Un utilisateur de cette base de données doit pouvoir récupérer des informations, mais aussi ajouter des entrées dans certaines tables afin de gérer les ressources, les adhérents ou les emprunts.  
Cependant, une bonne partie des interventions sur la base de données doivent manipuler plusieurs tables ou effectuer des vérifications.  
Une interface utilisateur devrait notamment disposer des fonctions suivantes.  

Get_donnes_adherent(): Permet de récupérer une synthèse des informations d'un adhérent et des emprunt effectués.

Emprunter() : Permet d'emprunter un exemplaire d'une ressource si elle est disponible. Crée une entrée dans la table Prêt. Nécessite les informations sur l'exemplaire et sur l'adhérent.  
Rendre() : Permet de rendre un exemplaire d'une ressource. Complète le champ "rendu_le" de l'entrée dans la table Prêt. Nécessite les informations sur le prêt (exemplaire, ressource, adhérent) et l'etat de l'exemplaire. Crée une sanction et suspend l'adhérent si dégradation, perte ou retard.  
Rembourser() : Permet de rembourser un exemplaire perdu ou endommagé  

Ajouter_livre(): Crée une nouvelle ressource de type livre.  
Ajouter_oeuvre_musicale(): Crée une nouvelle ressource de type oeuvre musicale.  
Ajouter_film(): Crée une nouvelle ressource de type film.  
Ajouter_exemplaire(): Ajouter un exemplaire d'une ressource  

Ajouter_contributeur(): Ajouter un contributeur à la table des contributeurs existants  
Ajouter_editeur(): Ajoute un éditeur à la table des éditeurs existants  
Ajouter_genre(): Ajoute un genre à la table des genre existants  

Creer_adherent(): Crée un nouvel adhérent à la bibliothèque.  
Blackliser(): Permet de blacklister un adhérent.  
Fermer_compte(): Met à jour la valeur de "date_fin_adhesion" d'une entrée de la table Adhérent, cela permet de garder une trace des anciens adhérents tout en sachant que leur compte est inactif.  

Creer_personnel(): Permet de créer un nouveau membre du personnel  
Retirer_personnel(): Retire un membre du personnel  

## Hypothèses réalisées 

1. Liste de contributeur (pour les enfants de Ressources) non nulle : une ressource ne peut pas être créée par personne. Pour un contributeur non connu, on notera "NC".
2. On choisi l'héritage pour les types Livre, Film et Musique car une grande partie des attributs sont commun à Ressource.
3. Il est possible d'avoir le même nom à la fois dans Réalisateur/Acteur et dans Composiiteur/Interprètes : un Contributeur peux avoir deux rôles.
4. Il est par contre impossible d'avoir 2 fois le même Contributeur dans une même liste.
