# Modèle Logique de Données

## Héritage presque complet par classe mère

- **Ressources**(#Code : Serial, Titre : varchar, Date_apparition : date, Editeur => Editeurs, Genre => Genres, Code_classification : varchar, ISBN : varchar, Resume : varchar, Langue : varchar, Duree : integer, Synopsis : varchar) *avec Date_apparition NOT NULL et Editeur NOT NULL et Genre NOT NULL et Code_classification NOT NULL*

### Contraintes :

	- Livres
	(ISBN IS NOT NULL AND Resume IS NOT NULL AND Langue IS NOT NULL AND Duree IS NULL AND Synopsis IS NULL) OR
	- Films
	(Langue IS NOT NULL AND Duree IS NOT NULL AND Synopsis IS NOT NULL AND ISBN IS NULL AND RESUME IS NULL) OR
	- Musiques
	(Duree IS NOT NULL AND ISBN IS NULL AND Resume IS NULL AND Langue IS NULL AND Synopsis IS NULL)

## Héritage par la classe fille

- **Adhérents**(#Login : varchar, Mot_de_passe : varchar, Nom : varchar, Prenom : varchar, Adresse : varchar, Email : varchar, Date_naissance : date, Numero_telephone : varchar, Date_fin_adhesion : date, Blackliste : booléen) *avec (Nom, Prenom, Adresse) KEY et Email UNIQUE NOT NULL*

- **Personnels**(#Login : varchar, Mot_de_passe : varchar, Nom : varchar, Prenom : varchar, Adresse : varchar, Email : varchar) avec (Nom, Prenom, Adresse) *avec KEY et Email UNIQUE NOT NULL*

### Contraintes :

	Jointure(Adherents, Personnels, Adherents.login = Personnels.login) = NULL

## Héritage presque complet par classe mère

- **Sanctions**(id_pret : integer, #id_sanction : varchar, Type : {perte, degradation, retard}, Duree_sanction : date, Date : date, Rembourse : booléen)

### Contraintes:

	- Type NOT NULL
	- (Duree_sanction IS NOT NULL AND Date IS NOT NULL AND Rembourse IS NULL) OR (Duree_sanction IS NULL AND Date IS NULL AND Rembourse IS NOT NULL)


## Autres classes

- **Contributeurs**(#id : Serial, Nom : varchar, Prenom : varchar, Date_naissance : date, Nationalite : varchar) *avec tout NOT NULL*

- **Contribue**(#id_contrib => Contributeurs, #id_ressource => Ressources, #Role : enum{auteur, compositeur, interprete, réalisateur, acteur})

- **Editeurs**(#Nom : varchar)

- **Genres**(#Nom : varchar)

- **Prets**(#id_pret : serial, id_exemplaire => Exemplaires.id, id_ressource => Exemplaires.id_ressource, Login_adherent => Adherent, Date_pret : date, Duree_pret : integer, Date_rendu : date) *avec Date_rendu > Date_pret*

- **Exemplaires**(#id_ressource => Ressources, #id : integer, Etat : enum{neuf, bon, abime, perdu}) *avec id LOCAL KEY*
