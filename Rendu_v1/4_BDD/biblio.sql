--TYPES

CREATE TYPE exemplaires_etat AS ENUM ('neuf', 'bon', 'abime', 'perdu');
CREATE TYPE sanctions_etat AS ENUM ('perte', 'degradation', 'retard');
CREATE TYPE contribue_role AS ENUM ('auteur', 'compositeur', 'interprete', 'realisateur', 'acteur');

--TABLES

CREATE TABLE Editeurs(
	nom VARCHAR NOT NULL,
	PRIMARY KEY (nom)
);


CREATE TABLE Genres (
	nom VARCHAR NOT NULL,
	PRIMARY KEY (nom)
);


CREATE TABLE Ressources(
	code SERIAL NOT NULL,
	titre VARCHAR NOT NULL,
	date_apparition DATE NOT NULL,
	editeur VARCHAR NOT NULL,
	genre VARCHAR NOT NULL,
	code_classification VARCHAR NOT NULL,
	isbn VARCHAR,
	resume TEXT,
	langue VARCHAR,
	duree INTEGER,
	synopsis TEXT,
	PRIMARY KEY (code),
	UNIQUE (isbn, editeur),
	FOREIGN KEY (editeur) REFERENCES Editeurs(nom),
	FOREIGN KEY (genre) REFERENCES Genres(nom),
	CHECK ((isbn IS NOT NULL AND resume IS NOT NULL AND langue IS NOT NULL AND duree IS NULL AND synopsis IS NULL) OR (langue IS NOT NULL AND duree IS NOT NULL AND synopsis IS NOT NULL AND isbn IS NULL AND resume IS NULL) OR (duree IS NOT NULL AND isbn IS NULL AND resume IS NULL AND langue IS NULL AND synopsis IS NULL))
);

CREATE VIEW vueRessource AS
SELECT code, titre, date_apparition, editeur, genre, code_classification, isbn, resume, langue, duree, synopsis,
CASE 
WHEN isbn IS NOT NULL THEN 'Livres'
WHEN synopsis IS NOT NULL THEN 'Films'
ELSE 'Musiques'
END AS type_Ressources
FROM Ressources;


CREATE TABLE Adherents(
	login VARCHAR UNIQUE NOT NULL,
	mot_de_passe VARCHAR NOT NULL,
	nom VARCHAR NOT NULL,
	prenom VARCHAR NOT NULL,
	adresse VARCHAR NOT NULL,
	email VARCHAR UNIQUE NOT NULL,
	date_naissance DATE NOT NULL,
	numero_telephone VARCHAR NOT NULL,
	date_fin_adhesion DATE NOT NULL,
	blackliste BOOLEAN NOT NULL,
	PRIMARY KEY (login)
);


CREATE TABLE Personnels (
	login VARCHAR UNIQUE NOT NULL,
	mot_de_passe VARCHAR NOT NULL,
	nom VARCHAR NOT NULL,
	prenom VARCHAR NOT NULL,
	adresse VARCHAR NOT NULL,
	email VARCHAR UNIQUE NOT NULL,
PRIMARY KEY (login)
);


CREATE TABLE Contributeurs (
	id SERIAL NOT NULL,
	nom VARCHAR NOT NULL,
	prenom VARCHAR NOT NULL,
	date_naissance DATE NOT NULL,
	nationalite VARCHAR NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE Contribue (
	id_contrib INTEGER NOT NULL,
	id_ressource INTEGER NOT NULL,
	role contribue_role NOT NULL,
	PRIMARY KEY (id_contrib, id_ressource, role),
	FOREIGN KEY (id_contrib) REFERENCES Contributeurs(id),
	FOREIGN KEY (id_ressource) REFERENCES Ressources(code)
);


CREATE TABLE Exemplaires (
	id_ressource INTEGER NOT NULL,
	id SERIAL NOT NULL,
	etat exemplaires_etat NOT NULL,
	PRIMARY KEY (id_ressource, id),
	FOREIGN KEY (id_ressource) REFERENCES Ressources(code)
);

CREATE TABLE Prets(
	id_pret SERIAL NOT NULL,
	id_exemplaire INTEGER NOT NULL,
	id_ressource INTEGER NOT NULL,
	login_adherent VARCHAR NOT NULL,
	date_pret DATE NOT NULL,
	duree_pret INTEGER NOT NULL,
	date_rendu DATE,
	PRIMARY KEY (id_pret),
	FOREIGN KEY (id_exemplaire, id_ressource) REFERENCES Exemplaires(id, id_ressource),
	CHECK(date_rendu>date_pret)
);


CREATE TABLE Sanctions (
	id_pret INTEGER NOT NULL,
	id_sanction SERIAL NOT NULL,
	Type sanctions_etat NOT NULL,
	duree_sanction INTEGER,
	Date DATE,
	rembourse BOOLEAN,
	PRIMARY KEY (id_sanction),
	FOREIGN KEY (id_pret) REFERENCES Prets(id_pret),
	CHECK((duree_sanction IS NOT NULL AND Date IS NOT NULL AND rembourse IS NULL) OR (duree_sanction IS NULL AND Date IS NULL AND rembourse IS NOT NULL))
);

CREATE VIEW vueSanctions AS
SELECT id_pret, id_sanction, Type, duree_sanction, Date, rembourse,
CASE 
WHEN duree_sanction IS NOT NULL THEN 'Temporaire'
ELSE 'A_rembourser'
END AS type_Sanction
FROM Sanctions;
