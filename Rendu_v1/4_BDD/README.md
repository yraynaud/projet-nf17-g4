# Base de données 

Le dossier est constitué de deux fichiers : 
1. [biblio.sql](biblio.sql) : SQL de création de la base de données, table par table.
2. [insert.sql](insert.sql) : SQL d'insertion de données de test. Si vous rencontrez une erreur lors de l'éxecution de la totalité de ce fichier, veuillez exécuter les commandes table par table.
