--Contributeurs

INSERT INTO Contributeurs("nom", "prenom", "date_naissance", "nationalite") VALUES
    ('Arouet', 'François-Marie', '1694-11-21', 'Française'),
    ('Shakespeare', 'William', '1564-04-26', 'Anglaise'),
    ('Tchaïkovski', 'Piotr', '1840-04-25', 'Russe'),
    ('Bach', 'Jean-Sébastien', '1685-03-31', 'Saxe'),
    ('Rowling', 'Joanne', '1965-07-31', 'Anglaise'),
    ('Lucas', 'George', '1944-05-14', 'Américaine');

--Genres

INSERT INTO Genres VALUES
    ('Science Fiction'),
    ('Documentaire'),
    ('Horreur'),
    ('Classique');

--Éditeurs

INSERT INTO Editeurs VALUES
    ('Hachette'),
    ('Flammarion'),
    ('Metro-Goldwyn-Mayer'),
    ('Pathé'),
    ('Universal Music Group'),
    ('PolyGram');

--Personnels

INSERT INTO Personnels VALUES 
    ('amichot', 'mdp123', 'Michot', 'Andre', '2 rue de la ferme, 75001, Paris', 'amicho@gmail.com'), 
    ('tparisis', 'mdp12', 'Parisis', 'Thibaut', '10 rue de Paris, 75012, Paris', 'tparisis@gmail.com'),
    ('pvalue', 'password', 'Value', 'Paul', '15 rue des paquerettes, 75003, Paris', 'pvalue@gmail.com'), 
    ('cparo', 'pwd', 'Paro', 'Charles', '23 rue de l aune, 75008, Paris', 'cparo@gmail.com');

--Adherents

INSERT INTO Adherents VALUES
    ('mdupont', 'monmdp', 'Dupont', 'Michel', '20 avenue de la republique, 75012, Paris', 'mdupont@gmail.com', '1980/06/06', '+33654548554', '2020/09/02', False),
    ('mpaul', 'supermdp', 'Paul', 'Michel', '20 rue des exemples, 75002, Paris', 'mpaul@gmail.com', '1990/08/01', '+33624151454', '2020/10/2', True),
    ('jpierre', 'monsupermdp', 'Pierre', 'Jacques', '5 rue de l eglise, 75006, Paris', 'jpierre@gmail.com', '1992/06/04', '+33666641202', '2020/10/20', False),
    ('jdutrou', 'superpwd', 'Dutrou', 'Jean', '5 avenue des champs, 75006, Paris', 'jdutrou@gmail.com', '1994/03/01', '+33624162144', '2020/10/2', False);

--Ressources

    --Film

INSERT INTO Ressources("titre", "date_apparition", "editeur", "genre", "code_classification", "langue", "duree", "synopsis") VALUES
    ('Star Wars', '1977-01-01', 'Pathé', 'Science Fiction', '132C42', 'Français', 123, 'Blablablabla');

    --Musique

INSERT INTO Ressources("titre", "date_apparition", "editeur", "genre", "code_classification", "duree") VALUES
    ('Le Lac des Cygnes', '1877-03-04', 'Universal Music Group', 'Classique', '567B79', 150);

    --Livre

INSERT INTO Ressources("titre", "date_apparition", "editeur", "genre", "code_classification", "isbn", "resume", "langue") VALUES
    ('Harry Potter', '1995-01-01', 'Hachette', 'Science Fiction', '540E42', 'AAA5042', 'Blablablabla', 'Français');

--Contribue

INSERT INTO Contribue VALUES
    (1, 1, 'acteur'),
    (3, 2, 'compositeur'),
    (5, 3, 'auteur');

--Exemplaires

INSERT INTO exemplaires(id_ressource, etat) VALUES
    (1, 'neuf'),
    (1,'neuf'),
    (1, 'abime'),
    (2, 'perdu'),
    (3, 'bon');

--Prets

INSERT INTO Prets ("id_exemplaire", "id_ressource", "login_adherent", "date_pret", "duree_pret", "date_rendu") VALUES
    (4, 2, 'mdupont', '2020-04-1', '7', '2020-04-8'),
    (3, 1, 'mpaul', '2020-04-2', '7', '2020-04-23'),
    (2, 1, 'jpierre', '2019-08-17', '7', NULL),
    (1, 1, 'jdutrou', '2018-11-3', '7', NULL);

--Sanctions

    --A_rembourser

INSERT INTO Sanctions ("id_pret", "type", "rembourse") VALUES
    (1, 'degradation', false);

INSERT INTO Sanctions ("id_pret", "type", "rembourse") VALUES
    (3, 'perte', false);

    --Temporaires

INSERT INTO Sanctions ("id_pret", "type", "date", "duree_sanction") VALUES
    (2, 'retard', NOW(), 15);
