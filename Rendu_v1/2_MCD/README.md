# Modèle Conceptuel de Données

Le dossier est constitué de trois fichiers : 
1. [UML_plantUML](UML_plantUML) : fichier au format [plantUML](https://plantuml.com/fr/) pour la création du MCD
2. [plantUML.png](plantUML.png) : rendu final du MCD
3. [UML.png](UML.png) : screenshot de l'UML réalisé collaborativement à l'aide l'outil [draw.io](https://app.diagrams.net/)